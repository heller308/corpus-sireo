/**
 * @Name: GeocodeResponse
 * @Description: This class handles
 * handle the response from geocode API
 */ 
public class GeocodeResponse{
    public cls_results[] results {get;set;}
    public String status;   //OK

    public class cls_results {
        public String formatted_address                    {get;set;}
        public cls_geometry geometry                       {get;set;}
        public List<Address_components> address_components {get;set;}
    }
    
    public class Address_components {
		public String long_name     {get;set;}
		public String short_name    {get;set;}
		public List<String> types   {get;set;}
	}
	
    public class cls_geometry {
        public cls_location location    {get;set;}
        public String location_type     {get;set;}
    }
    
    public class cls_location {
        public Double lat  {get;set;}
        public Double lng  {get;set;}
    }
    
    public static GeocodeResponse parse(String json){
        return (GeocodeResponse) System.JSON.deserialize(json, GeocodeResponse.class);
    }
}