public class PlacesAPIResponse{
    public cls_results[] results                {get;set;}
    public String status                        {get;set;}

    public class cls_results {
        
        public String formatted_address         {get;set;}          //Ahinsa Cir, Ashok Nagar, Jaipur, Rajasthan 302001, India
        public cls_geometry geometry            {get;set;}
        
        public String icon                      {get;set;}          //https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png
        
        public String name                      {get;set;}          //Funduz Burger's and Vadapav
        
        public String[] types                   {get;set;}          //type of the place returned(restaurent, shop etc)
        
        public String place_id                  {get;set;}          //ChIJyWEHuEmuEmsRm9hTkapTCrk
    }
    
    public class cls_geometry {
        public cls_location location            {get;set;}
    }
    
    public class cls_location {
        public Double lat                       {get;set;}          //26.9145048
        
        public Double lng                       {get;set;}          //75.8054375
    }
    
    public static PlacesAPIResponse parse(String json){
        return (PlacesAPIResponse) System.JSON.deserialize(json, PlacesAPIResponse.class);
    }
}