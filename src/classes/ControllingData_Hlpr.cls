/**
 * @Name GeocodeDataAndAction
 * @Description it lists all the data required for fields mapping
 * like Object, Fieldsets, Fields
 */ 
public class ControllingData_Hlpr{
        public String objectName                    {get;set;}
        public String fieldSetName                  {get;set;}
        public String houseNumber                   {get;set;}
        public String street                        {get;set;}
        public String zipCode                       {get;set;}
        public String city                          {get;set;}
        public String state                         {get;set;}
        public String country                       {get;set;}
        public String name                          {get;set;}
        public String geoCodeStatus                 {get;set;}
        public String geoCodeLatitude               {get;set;}
        public String geoCodeLongitude              {get;set;}
        public String geoCodeAccuracy               {get;set;}
        public String geoCodeError                  {get;set;}
        public String infoWindowFieldset            {get;set;}
        
        public ControllingData_Hlpr(String objectName, String fieldSetName, String street, String zipCode, String city, String state, String country, String name, 
            String geoCodeStatus, String geoCodeLatitude, String geoCodeLongitude, String geoCodeAccuracy, String geoCodeError, String infoWindowFieldset,String houseNumber){
            this.objectName         =   objectName;
            this.fieldSetName       =   fieldSetName;
            this.houseNumber        =   houseNumber;
            this.street             =   street;
            this.zipCode            =   zipCode;
            this.city               =   city;
            this.state              =   state;
            this.country            =   country;
            this.name               =   name;
            this.geoCodeStatus      =   geoCodeStatus;
            this.geoCodeLatitude    =   geoCodeLatitude;
            this.geoCodeLongitude   =   geoCodeLongitude;
            this.geoCodeAccuracy    =   geoCodeAccuracy;
            this.geoCodeError       =   geoCodeError;
            this.infoWindowFieldset =   infoWindowFieldset;
        }
        public ControllingData_Hlpr(){
            this.objectName         =    '';
            this.fieldSetName       =    '';
            this.houseNumber             =    '';
            this.street             =    '';
            this.zipCode            =    '';
            this.city               =    '';
            this.state              =    '';
            this.country            =    '';
            this.name               =    '';
            this.geoCodeStatus      =    '';
            this.geoCodeLatitude    =    '';
            this.geoCodeLongitude    =    '';
            this.geoCodeAccuracy    =    '';
            this.geoCodeError       =    '';
            this.infoWindowFieldset =    '';
        }
    }