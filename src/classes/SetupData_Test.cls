@isTest
public class SetupData_Test {
    
    @isTest static void testSetupData(){
        CSMA__setupData__c setupPage = TestDataUtility.getSetup;
        CSMA__Geocding_and_Actions__c t = TestDataUtility.geocodeAndAction;
        Test.startTest();
        
        SetupData_Ctlr setupController = new SetupData_Ctlr();
        List<SelectOption> listSearchFor = setupController.searchFor;
        List<SelectOption> listKmOrMile = setupController.kmOrMile;
        SetupData_Ctlr.saveInitialMarkerData('test');
        SetupData_Ctlr.saveLinkToVF('test');
        SetupData_Ctlr.saveNewLocationTooltip('test');
        SetupData_Ctlr.saveExistingTooltip('test');
        SetupData_Ctlr.deleteLinkToVF();
        SetupData_Ctlr.saveThirdMarkerData('testing122344');
        SetupData_Ctlr.saveConditionalMarkerData('testing122344');
        List<SelectOption> ls = setupController.UserList;
        
        system.assertNotEquals(null, listSearchFor);
        system.assertNotEquals(null, listKmOrMile);
        SetupData_Ctlr.createCronJob('daily','','19:0');
        Test.stopTest();
        SetupData_Ctlr.MultiSelectOption mo = new SetupData_Ctlr.MultiSelectOption('test','test');
    }
    
    @isTest static void testUpdateSetupData(){
        CSMA__setupData__c setupPage = TestDataUtility.getSetup;
        CSMA__Geocding_and_Actions__c t = TestDataUtility.geocodeAndAction;
        Test.startTest();
        SetupData_Ctlr.updateData('162.1', 'Miles', 'All Records','CSMA__cs_Street__c');
        CSMA__setupdata__c dataUpdate = [SELECT ID, CSMA__Search_For__c, CSMA__search_Radius__c, CSMA__Radius_Unit__c FROM CSMA__setupdata__c LIMIT 1];
        system.assertEquals('All Records', dataUpdate.CSMA__Search_For__c);
        system.assertEquals(162.1, dataUpdate.CSMA__search_Radius__c);
        system.assertEquals('Miles', dataUpdate.CSMA__Radius_Unit__c);
        Test.stopTest();
    }
    
    @isTest static void testGeocoding() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.startTest();
        CSMA__setupData__c  setupPage   = TestDataUtility.getSetup;
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
        
        List<CSMA__Controlling_Object__c> properties = TestDataUtility.getNeedGeocodeProperties;
        String isStartSuccesfully = SetupData_Ctlr.startGeocoding();
        system.assertEquals('Geocode Started', isStartSuccesfully);
        
        SetupData_Ctlr.emailUser('true', 'true','test');

        SetupData_Ctlr setupController = new SetupData_Ctlr();
        setupController.sendEmailNotification();
        setupController.saveFunc();

        List<SelectOption> availableObjects = setupController.availableObjects;
        List<SelectOption> availableFields  = setupController.availableFields;
        List<SelectOption> vfpages = setupController.vfPages;
        //List<SelectOption> availableLocationFields    = setupController.availableLocationFields;
        
        setupController.selectValue = 'any';
        List<SetupData_Ctlr.fieldSetWrapper> fieldsets  = SetupData_Ctlr.getFieldsets(geocodeAndAction.CSMA__Controlling_Object__c);
        List<SetupData_Ctlr.FiltersWrapper> availableFieldsMap  = SetupData_Ctlr.availableFields(geocodeAndAction.CSMA__Controlling_Object__c);
        Boolean status = SetupData_Ctlr.isGeocodingRunning();
        
        String data = '{"zipCode":"CSMA__cs_PLZ__c","street":"CSMA__cs_Street__c","state":"CSMA__State__c","objectName":"CSMA__Controlling_Object__c","name":"Name","infoWindowFieldset":"CSMA__testSet","geoCodeStatus":"CSMA__Geocoding_Status__c","geoCodeLatitude":"CSMA__Location__Latitude__s","geoCodeLongitude":"CSMA__Location__Longitude__s","geoCodeError":"CSMA__Geocoding_Error__c","geoCodeAccuracy":"CSMA__cs_Geocode_Quality__c","fieldSetName":"CSMA__Hello_test","country":"CSMA__cs_Country__c","city":"CSMA__cs_City__c"}';
        String result = SetupData_Ctlr.geocodeDataAndActiondata(data);
        // SetupData_Ctlr.relatedRecordData(geocodeAndAction.CSMA__Controlling_Object__c,'testSet','');
        SetupData_Ctlr.relatedRecordData(geocodeAndAction.CSMA__Related_Record_Condition1__c);
        SetupData_Ctlr.deleteRelatedRecordData();
        Test.stopTest();
    }

}