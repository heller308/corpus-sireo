public class CheckPermissionUtility {
 
    public static DescribeSObjectResult getDescribeSObjectResult(String objectName){
        return Schema.getGlobalDescribe().get(objectName).getDescribe();
    }
    
    public static DescribeFieldResult getDescribeFieldResult(DescribeSObjectResult obj, String fieldName){
        return obj.fields.getMap().get(fieldName).getDescribe();
    }
    
    public static Boolean checkAllPermissionsField(DescribeFieldResult dfr){
        return (dfr.isAccessible()  && dfr.isCreateable() && dfr.isUpdateable());
    }
    
    public static Boolean checkReadPermissionsField(DescribeFieldResult dfr){
        return (dfr.isAccessible());
    }
    
    public static Boolean checkAllPermissionsObject(DescribeSObjectResult dsr){
        return (dsr.isAccessible() && dsr.isCreateable() && dsr.isUpdateable() && dsr.isQueryable());
    }
    
    public static Boolean checkReadPermissionsObject(DescribeSObjectResult dsr){
        return (dsr.isAccessible());
    }
}