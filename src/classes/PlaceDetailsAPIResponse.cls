public class PlaceDetailsAPIResponse{
    public cls_result result;
    public String status;   //OK
    
    public class cls_result {
        public cls_address_components[]  address_components;
        public String formatted_address;    //48 Pirrama Road, Pyrmont NSW, Australia
        public String name; //Google Sydney
    }
    public class cls_address_components {
        public String long_name;    //48
        public String short_name;   //48
        public String[] types;
    }
    
    public static PlaceDetailsAPIResponse parse(String json){
        return (PlaceDetailsAPIResponse) System.JSON.deserialize(json, PlaceDetailsAPIResponse.class);
    }
}