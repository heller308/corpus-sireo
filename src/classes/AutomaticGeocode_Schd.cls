global with sharing class AutomaticGeocode_Schd implements Schedulable {
	
	global void execute(SchedulableContext SC) {
		Integer iJobs = [Select id From AsyncApexJob where Status = 'Processing' or status = 'Queued'].size();
		System.debug('iJobs :'+iJobs);
        if(iJobs < 5) {
            Database.executeBatch(new MannualGeocode_Btch(),10) ;
        }
	}
}