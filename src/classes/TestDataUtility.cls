@isTest
public with sharing class TestDataUtility {
    public static CSMA__Controlling_Object__c createProperty{
        get{
            if(createProperty == null){
                createProperty          =   new CSMA__Controlling_Object__c(Name='test',CSMA__cs_City__c = 'testCity',
                CSMA__cs_Street__c = 'testStreet',
                CSMA__cs_PLZ__c = '302017',
                CSMA__State__c = 'testState',
                CSMA__cs_Country__c = 'testCountry',
                CSMA__Geocoding_Status__c = 'Geocoded',
                CSMA__Location__Latitude__s = 55.3781,
                CSMA__Location__Longitude__s = 3.4360
                );
                insert createProperty;
            }
            return createProperty;
        }
        set{
            
        }
    }
    
        
    public static CSMA__Geocding_and_Actions__c geocodeAndAction{
        get{
            if(geocodeAndAction == null){
                geocodeAndAction = new CSMA__Geocding_and_Actions__c(CSMA__Controlling_Object__c = 'CSMA__Controlling_Object__c',
                                            Name = 'Name',
                                            CSMA__Name__c = 'Name',
                                            CSMA__City__c = 'CSMA__cs_City__c',
                                            CSMA__New_PropertyVF__c = 'CSMA__New_PropertyVF__c',
                                            CSMA__Filter_Fields1__c='CSMA__cs_City__c,CSMA__cs_Geocode_Quality__c,CreatedDate',
                                            CSMA__Country__c = 'CSMA__Cadastral_Number__c',
                                            CSMA__Geocode_Quality__c = 'CSMA__cs_Geocode_Quality__c',
                                            CSMA__Geocode_Error__c = 'CSMA__Geocoding_Error__c',
                                            CSMA__Geocode_Status__c = 'CSMA__Geocoding_Status__c',
                                            CSMA__Info_Window_Field_Set__c = 'CSMA__testSet',
                                            CSMA__State__c = 'CSMA__State__c',
                                            CSMA__Street__c = 'CSMA__cs_Street__c',
                                            CSMA__ZIP_Code__c = 'CSMA__cs_PLZ__c',
                                            CSMA__Latitude__c = 'CSMA__Location__Latitude__s',
                                            CSMA__Longitude__c = 'CSMA__Location__Longitude__s',
                                            CSMA__Link_to_Visualforce__c = '{"eName":"TEST BUTTONEN","gName":"TEST BUTTONDE","vfName":"MapView"}',
                                            CSMA__House_Number__c = 'CSMA__House_Number__c',
                                            CSMA__Initial_Marker_Color__c = '{"defaultColor":true,"color":"#e8584f"}',
                                            CSMA__Conditional_Marker_Color1__c = '{"defaultColor":false,"color":"#1a7a49","condition":{"field":"CSMA__cs_Country__c","type":"PICKLIST","fieldLabel":" Country ","operator":"=","operatorLabel":"equals","value":"Germany","valueLabel":"Germany"}}',
                                            CSMA__Third_Maker_Color__c = '{"defaultColor":false,"color":"#0400a8"}',
                                            CSMA__Related_Record_Condition1__c = '{"objName":"CSMA__Controlling_Child_Object__c","fieldsetName":"CSMA__Test_Field_Set","condition":{"field":"CSMA__State__c","type":"PICKLIST","fieldLabel":"State","operator":"=","operatorLabel":"equals","value":"Hamburg","valueLabel":"Hamburg"}}',
                                            CSMA__New_Loaction_Marker_Tooltip__c = '{"combination":true,"field":"Address,geocoordinates"}',
                                            CSMA__Existing_Marker_Tooltip1__c = 'Hi <<Name>><<CSMA__cs_Asset_Class_Object_Type__c>>'
                                            );
                
                insert geocodeAndAction;
            }
            return geocodeAndAction;
        }
        set{
            
        }
    }
    
    public static List<CSMA__Controlling_Object__c> getProperties{
        get{
            if(getProperties == null){
                getProperties = new List<CSMA__Controlling_Object__c>();
                for(integer i=0; i < 10 ; i++){
                    CSMA__Controlling_Object__c property =  new CSMA__Controlling_Object__c(CSMA__cs_City__c = 'testCity'+i,
                                                            CSMA__cs_Street__c = 'testStreet'+i,
                                                            CSMA__cs_PLZ__c = '302017',
                                                            CSMA__State__c = 'testState'+i,
                                                            CSMA__cs_Country__c = 'testCountry'+i,
                                                            CSMA__Geocoding_Status__c = 'Geocoded');
                    getProperties.add(property);
                }
                insert getProperties;
            }
           return getProperties;
        }
        set{
            
        }
    }
    
    public static List<CSMA__Controlling_Object__c> getNeedGeocodeProperties{
        get{
            if(getNeedGeocodeProperties == null){
                getNeedGeocodeProperties = new List<CSMA__Controlling_Object__c>();
                for(integer i=0; i < 1 ; i++){
                    CSMA__Controlling_Object__c property=   new CSMA__Controlling_Object__c(Name = 'test'+i,
                                                                CSMA__cs_City__c = 'testCity'+i,
                                                                CSMA__cs_Street__c = 'testStreet'+i,
                                                                CSMA__cs_PLZ__c = '302017',
                                                                CSMA__State__c = 'testState'+i,
                                                                CSMA__cs_Country__c = 'testCountry'+i,
                                                                CSMA__Geocoding_Status__c = 'Needs Geocoding');
                    getNeedGeocodeProperties.add(property);
                }
                insert getNeedGeocodeProperties;
            }
            return getNeedGeocodeProperties;
        }
        set{
            
        }
    }
    
    public static CSMA__setupData__c getSetup{
        get{
            if(getSetup == null){
                getSetup = new CSMA__setupData__c(CSMA__Search_For__c = 'Own Records',
                                        CSMA__search_Radius__c = 160.1,
                                        CSMA__Radius_Unit__c = 'Miles',
                                        CSMA__Notify_Admin__c = false,
                                        CSMA__NotifyMe__c = true,
                                        CSMA__Filter_Fields__c = 'CSMA__State__c,CSMA__cs_City__c',
                                        CSMA__Related_Record_Object__c = 'CSMA__Controlling_Child_Object__c',
                                        CSMA__Related_Record_FieldSet__c = 'CSMA__Test_Field_Set',
                                        CSMA__Related_Record_Condition__c = '{"field":"CreatedDate","type":"DATETIME","fieldLabel":"Created Date","operator":"=","operatorLabel":"equals","value":"09.08.2017 18:08","valueLabel":""}');
                insert getSetup;
            }
            return getSetup;
        }
        set{
            
        }
    }
}