@isTest
private class Geocode_Test {
	
	@isTest static void testGeocode() {
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
		CSMA__Controlling_Object__c property = TestDataUtility.createProperty;
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
		Geocode_Ws.GeocodeObject(property);

		Test.stopTest();
	}	
	 
	@isTest static void testGeocodeAmbigous() {
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
		CSMA__Controlling_Object__c property = TestDataUtility.createProperty;
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorAmbigousResult());
		Test.startTest();
		Geocode_Ws.GeocodeObject(property);

		Test.stopTest();
	}
	
	@isTest static void testGeocodeNoResult() {
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
		CSMA__Controlling_Object__c property = TestDataUtility.createProperty;
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorNoResult());
		Test.startTest();

		Geocode_Ws.GeocodeObject(property);

		Test.stopTest();
	}
}