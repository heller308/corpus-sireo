@isTest
public class MapView_Test {
 
    @isTest static void testMapView(){
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
        CSMA__Controlling_Object__c property = TestDataUtility.createProperty;
        CSMA__setupData__c setupData = TestDataUtility.getSetup;
        CSMA__Controlling_Child_Object__c offer = new CSMA__Controlling_Child_Object__c();
            offer.Name = 'test';
            offer.CSMA__offers__c = property.id;
            offer.CSMA__Record_Details__c = 'test11';
            insert offer;
        MapView_Ctlr.getObjectOffers(property.id);
        List<CSMA__Controlling_Object__c> properties = TestDataUtility.getProperties;
        String s = MapView_Ctlr.checkPlace('55.3781', '3.4360');
        
        PageReference pageRef = Page.MapView;
        Test.setCurrentPageReference(pageRef); 
        System.currentPageReference().getParameters().put('id',property.Id);
        MapView_Ctlr.FilterWrapper wrap = new MapView_Ctlr.FilterWrapper();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        MapView_Ctlr mapController = new MapView_Ctlr();
         // method which provides picklist to show the grouped data on js chart.
         mapController.returnAllPickListField();
         //calling js chart related Remote Method
         MapView_Ctlr.recordCountForChart(string.valueof(property.id),'CSMA__State__c');
        MapView_Ctlr.PropertyDetailsWrapper pdwObj = new MapView_Ctlr.PropertyDetailsWrapper('testStreet','testCity','testState','testZip','testCountry','testName','testHouseNumber');
        mapController.getPickListValues('CSMA__Geocoding_Status__c');
        mapController.filterApplied = '['+
            '{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"=","operatorLabel":"equals","value":"dgdfg"},'+
            '{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"starts with","operatorLabel":"starts with","value":"dgdfg"},'+
            '{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"Not like","operatorLabel":"not like","value":"dgdfg"},'+
            '{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"Like","operatorLabel":"like","value":"dgdfg"},'+
            '{"field":"IsDeleted","type":"BOOLEAN","fieldLabel":"Deleted","operator":"=","operatorLabel":"equals","value":"true"},'+
            '{"field":"createdDate","type":"DATETIME","fieldLabel":"Created Date","operator":"=","operatorLabel":"equals","value":"05.07.2017 12:21"},'+
            '{"field":"CSMA__Location__Latitude__s","type":"DOUBLE","fieldLabel":"Location(Latitude)","operator":"=","operatorLabel":"equals","value":"12.214152"}]';
        
        mapController.recordId=null;
        mapController.newLocation();
        String paddedString = mapController.padIntegerWithZero(1); 
        List<MapView_Ctlr.PropertyWrapper> wrapperProperties = MapView_Ctlr.getRelatedProperty('test','false',3,'false');
        
        
        // PlaceDetailsAPIResponse.cls_address_components test1 = new PlaceDetailsAPIResponse.cls_address_components();
        // test1.long_name = 'Delhi';
        // test1.short_name = 'DL';
        // test1.types  = {'political','administrative_area_level_1'};
        
        // PlaceDetailsAPIResponse.cls_address_components[] addrComp = new PlaceDetailsAPIResponse.cls_address_components[]{test1};
        // MapView_Ctlr.getLocationDetails('');
        String isSaved = MapView_Ctlr.saveSearchResult('testname', property.Id, '162.1', 'km', 'All Records', '55.3781123', '3.4360987','','[{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"=","operatorLabel":"equals","value":"dgdfg"}]','');
        system.assertEquals(true, isSaved!=null);
        
        List<CSMA__Search_Result__c> searchResults = MapView_Ctlr.searchResults();
        System.assertNotEquals(null, searchResults);
        mapController.reRenderFunction();
        Test.stopTest();
    }
    
    @isTest static void testMapViewwithoutProperty(){
        CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
        CSMA__setupData__c setupData = TestDataUtility.getSetup;
        List<CSMA__Controlling_Object__c> propList = TestDataUtility.getNeedGeocodeProperties;
        
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            MapView_Ctlr.createListViewForControllingObject('{"attributes":{"type":"CSMA__Controlling_Object__c","url":"/services/data/v40.0/sobjects/CSMA__Controlling_Object__c/a006F00002m6HmlQAE"},"Id":"a006F00002m6HmlQAE","Name":"testGermany","CSMA__Location__Latitude__s":35.2174032,"CSMA__Location__Longitude__s":-80.83046949999999,"CSMA__cs_City__c":"state","CSMA__State__c":"Baden-Wurttemberg","CSMA__Geocoding_Status__c":"Geocoded","LastModifiedDate":"01.05.2017 11:51","CSMA__cs_Country__c":"Albania","CSMA__cs_Asset_Class__c":"EUROPEAN_FOND"}','','Test','5.0');
            listView lv = [SELECT Id,Name FROM ListView WHERE SobjectType =:geocodeAndAction.CSMA__Controlling_Object__c limit 1];
            
            // MapView_Ctlr.getPlaceDetails('ChIJW9oBSwu2bTkRiPK_VSd7ncQ',false);
            // MapView_Ctlr.getPlaceDetails('ChIJW9oBSwu2bTkRiPK_VSd7ncQ',true);
            //MapView_Ctlr.controllingObjectListViews(String.valueof(lv.id));
            Utility.getFieldLabel(geocodeAndAction.CSMA__Controlling_Object__c,'Name','Field');
            Utility.getFieldLabelRemote(geocodeAndAction.CSMA__Controlling_Object__c,'Name','Field');
            // Utility.getLabelRemote(geocodeAndAction.CSMA__Controlling_Object__c,'Name','Field');
            Utility.getSetupDataLabel('Field','Name');
        Test.stopTest();
        MapView_Ctlr mapController = new MapView_Ctlr();
        mapController.filterApplied = '[{"field":"CSMA__cs_City__c","type":"STRING","fieldLabel":"City","operator":"=","operatorLabel":"equals","value":"dgdfg"}]';
        List<MapView_Ctlr.PropertyWrapper> wrapperProperties = MapView_Ctlr.getRelatedProperty('A-697, Malviya nagar,jaipur','false',3,'false');
        
        MapView_Ctlr.PropertyDetailsWrapper p = new MapView_Ctlr.PropertyDetailsWrapper();
        String isSaved = MapView_Ctlr.saveSearchResult('testname', '', '162.1', 'km', 'All Records', '55.3781123', '3.4360987','','','');
        Boolean deleted = MapView_Ctlr.deleteSearchResult(isSaved);
        system.assertEquals(true, isSaved!=null);
        List<CSMA__Search_Result__c> searchResults = MapView_Ctlr.searchResults();
        system.assertNotEquals(null, searchResults);
        mapController.reRenderFunction();
        MapView_Ctlr.deleteSearchResult((Id)isSaved);
        mapController.getControllingObjectExistingViews();
    }
    
    @isTest static void testMapForGoogleLocation(){
           CSMA__Geocding_and_Actions__c geocodeAndAction = TestDataUtility.geocodeAndAction;
           CSMA__setupData__c setupData = TestDataUtility.getSetup;
           PageReference pageRef = Page.MapView;
           Test.setCurrentPageReference(pageRef);
           
           System.currentPageReference().getParameters().put('placeDetails','ChIJLbZ-NFv9DDkRQJY4FbcFcgM');
           System.currentPageReference().getParameters().put('latitude','28.7040592');
           System.currentPageReference().getParameters().put('longitude','77.10249019999999');
           Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
           MapView_Ctlr mapController = new MapView_Ctlr();
           PlacesAPIResponse p = Geocode_Ws.getPlaces('Jaipur');
           Test.stopTest();
    }
}