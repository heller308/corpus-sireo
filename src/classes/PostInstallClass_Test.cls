@isTest
private class PostInstallClass_Test {
     @isTest
    static void testInstallScript() {
        PostInstallClass obj = new PostInstallClass();
        Test.testInstall(obj, null);
        List<CSMA__setupdata__c> setups = [SELECT Id FROM CSMA__setupdata__c LIMIT 1];
        System.assertEquals(1,setups.size());
    }
}