/**
 * @Name: PostInstallClass
 * @Description: This class handles
 * create default setup for App
 */
global class PostInstallClass implements InstallHandler {
    global void onInstall(InstallContext context) {
        List<CSMA__setupdata__c> setups = [SELECT Id FROM CSMA__setupdata__c LIMIT 1];
        if(setups.size()==0){
            CSMA__setupdata__c setup        = new CSMA__setupdata__c(
                CSMA__search_Radius__c = 5.0, 
                CSMA__Radius_Unit__c = 'KM',
                CSMA__Search_For__c = 'Own Records');
            insert setup;    
        }
    }
}