/**
 * @Name: MannualGeocode
 * @Description: This class handles Geocoding of Mapped object's data
 */
 
public class MannualGeocode_Btch implements Database.Batchable<sObject>,Database.stateful, Database.AllowsCallouts{

    ControllingData_Hlpr setupFieldData;
    Boolean isEmpty;
    public MannualGeocode_Btch(){
        isEmpty = false;
        setupFieldData = Utility.fillMappingValue();
    }
    
    /**
     *@Description mapping fields 
     * 
    **/
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String geoStatus    = 'Needs Geocoding';
        String query        = 'SELECT Id FROM ' + setupFieldData.objectName + ' WHERE (' + setupFieldData.geoCodeStatus + ' = :geoStatus OR ' + setupFieldData.geoCodeStatus + ' = null)';
        if(Test.isRunningTest()){
            query += ' LIMIT 10';
        }
        String query1        = 'SELECT Id FROM ' + setupFieldData.objectName + ' WHERE (' + setupFieldData.geoCodeStatus + ' = :geoStatus OR ' + setupFieldData.geoCodeStatus + ' = null) LIMIT 1';
        List<SObject> ls = Database.query(query1);
        System.debug('LS  :'+ls);
        System.debug('LS  :'+ls.isEmpty());
        System.debug('LS  :'+String.valueOf(ls));
        if(ls.isEmpty()){
            isEmpty = true;
            System.debug('in EMPTY');
        }
		return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<Id> objectIds = new List<Id>();
	    for(sObject obj : scope){
            objectIds.add((Id)obj.get('Id'));
	    }
        if(!objectIds.isEmpty()){
            List<sObject> controllingObject = fetchObjectProperties(objectIds, setupFieldData.objectName);
            if(!controllingObject.isEmpty()){
                for(sObject obj : controllingObject){
                    Geocode_Ws.GeocodeObject(obj); 
                }
                update controllingObject;
            }
        }
	}
    
    public list<sobject> fetchObjectProperties(List<id> objectIds, String objectName) {
        String query = 'SELECT Id, ' + setupFieldData.street + ', ' + setupFieldData.zipCode + ', ' + setupFieldData.houseNumber + ', ' + setupFieldData.country + ', ' + setupFieldData.city;
		if(setupFieldData.state!=null && setupFieldData.state!='')
		    query += ', ' + setupFieldData.state;
		query +=   ' FROM ' + setupFieldData.objectName + ' WHERE 	Id in: objectIds';
		list<sobject> objectsWithAddressDeatail = Database.query(query);
        return objectsWithAddressDeatail;
	}
	
	public void finish(Database.BatchableContext BC) {
	    System.debug('isEmpty :'+isEmpty);
	    if(isEmpty){
	        EmailTemplate templateId = [SELECT id FROM EmailTemplate WHERE name =: Label.cs_No_geocoding_status LIMIT 1];
            System.debug('templateIdIF :'+templateId.id);
            if(templateId != NULL){
                SetupData_Ctlr.sendEmailNotificationForTemplate(templateId);
            }
	    }else{
	        EmailTemplate templateId = [SELECT id FROM EmailTemplate WHERE name =: Label.cs_Email_Template LIMIT 1];
            System.debug('templateIdElse :'+templateId.id);
            if(templateId != NULL){
                SetupData_Ctlr.sendEmailNotificationForTemplate(templateId);
            }
	    }
	}
}