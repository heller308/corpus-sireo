public with sharing class Geocode_Ws {
    /**
     * @Name: Geocode
     * @Description: This class handles
     * Geocode details for controlling object's record
     */
     
      
    /**
     * @Description mapping of fields
     */
    static ControllingData_Hlpr setupFieldData;
	/**
     * @Description add geocode data to object record
     * @param ObjectName: Geocoding Object  
     */
	public static String GeocodeObject(sObject obj) {
	    setupFieldData = Utility.fillMappingValue();
	    DescribeSObjectResult accountDSR = CheckPermissionUtility.getDescribeSObjectResult(setupFieldData.objectName);
        DescribeFieldResult AcGeocoding_StatusDFR   = CheckPermissionUtility.getDescribeFieldResult(accountDSR, setupFieldData.geoCodeStatus);
        DescribeFieldResult AcGeocoding_ErrorDFR    = CheckPermissionUtility.getDescribeFieldResult(accountDSR, setupFieldData.geoCodeError);
        DescribeFieldResult AclatitudeDFR           = CheckPermissionUtility.getDescribeFieldResult(accountDSR, setupFieldData.geoCodeLatitude);
        DescribeFieldResult AclongitudeDFR          = CheckPermissionUtility.getDescribeFieldResult(accountDSR, setupFieldData.geoCodeLongitude);
        
        
        if(CheckPermissionUtility.checkAllPermissionsObject(accountDSR)
            &&  CheckPermissionUtility.checkAllPermissionsField(AcGeocoding_StatusDFR)
            &&  CheckPermissionUtility.checkAllPermissionsField(AcGeocoding_ErrorDFR)
            &&  CheckPermissionUtility.checkAllPermissionsField(AclatitudeDFR)
            &&  CheckPermissionUtility.checkAllPermissionsField(AclongitudeDFR)){
    		String message = '';
    		try {
    		    
                GeocodeResponse Coordinates = getSelectedCoordinates((String)obj.get(setupFieldData.street), (String)obj.get(setupFieldData.city), (String)obj.get(setupFieldData.zipCode), setupFieldData.state==null?'': (String)obj.get(setupFieldData.state), (String)obj.get(setupFieldData.country), (String)obj.get(setupFieldData.houseNumber));
                if(!Coordinates.results.isEmpty()){
                    if(Coordinates.status != 'OK' && (Coordinates.results[0].geometry.location.lat == null || Coordinates.results[0].geometry.location.lng == null)) {
                        Coordinates.status = 'AMBIGUOUS_ADDRESS'; //more than one result. for example: King st, London
                    }
                    
                    if(Coordinates.status == '' || (Coordinates.status == 'OK' && Coordinates.results[0].geometry.location.lat == null) || (Coordinates.status == 'OK' && Coordinates.results[0].geometry.location.lng == null)) {
                        Coordinates.status = 'COMMUNICATION_ERROR';
                    }
                    if(Coordinates.status != 'OK') {
                        if(Coordinates.status == 'ZERO_RESULTS' ||
                            Coordinates.status == 'AMBIGUOUS_ADDRESS') {
                            obj.put(setupFieldData.geoCodeStatus, 'Cannot Be Geocoded');
                            obj.put(setupFieldData.geoCodeError, Coordinates.status);
                        } else {
                            obj.put(setupFieldData.geoCodeStatus,'Needs Geocoding');
                            if(Coordinates.status.length() > 255) {
                                Coordinates.status = Coordinates.status.substring(0,255);
                            }
                            obj.put(setupFieldData.geoCodeError, Coordinates.status);
                        }
                        obj.put(setupFieldData.geoCodeLatitude, 0);
                        obj.put(setupFieldData.geoCodeLongitude, 0);
                    } 
                    else {
                        obj.put(setupFieldData.geoCodeError, null);
                        obj.put(setupFieldData.geoCodeStatus, 'Geocoded');
                        obj.put(setupFieldData.geoCodeAccuracy, Coordinates.results[0].geometry.location_type);
                        obj.put(setupFieldData.geoCodeLatitude, Coordinates.results[0].geometry.location.lat);
                        obj.put(setupFieldData.geoCodeLongitude,Coordinates.results[0].geometry.location.lng);
                    }
                    message = 'Record geocoded succesfully : ' + (String)obj.get('Id');                 		
                }else{		
                    message = 'no records';      
                    obj.put(setupFieldData.geoCodeStatus, 'Cannot Be Geocoded');
                    obj.put(setupFieldData.geoCodeError, Coordinates.status);           		
                }
            } catch(Exception e) {
                System.debug('ERROR : '+ e.getMessage());
    			obj.put(setupFieldData.geoCodeError, e.getMessage());	
    			Utility.trackError(e.getMessage());
                message = 'Exception occured while geocoding : ' + e.getMessage(); 
            }
            
            return message;
    	
    	}
    	else{
    	    Utility.trackError('permission issue in GeocodeObject()(Geocode_Ws)');
    	    return 'You don\'t have permission to access some of the required object/fields';
    	}
	}
	
	/**
     * @Description callout to google geocode API to fetch data
     * @param address components
     */
	public static GeocodeResponse getSelectedCoordinates(String street, String city, String zip, String state, String country,String houseNumber) {
        List<String> Coordinates = new List<String>();
        String url;
        
        if(street == null)  {street = '';}
        if(houseNumber == null)  {houseNumber = '';}
        if(city == null)    {city = '';}
        if(zip == null)     {zip = '';}
        if(state == null)   {state = '';}
        if(country == null) {country = '';}
        url = '/maps/api/geocode/json?address=';
        String lang = UserInfo.getLanguage();
        if(lang == 'de'){
            url = '/maps/api/geocode/json?language=de&address=';
        }
		
        url += EncodingUtil.urlEncode(houseNumber,'UTF-8') + ','+EncodingUtil.urlEncode(street,'UTF-8') + ',' + EncodingUtil.urlEncode(city,'UTF-8') + ',' +
               EncodingUtil.urlEncode(zip,'UTF-8') + ',' + EncodingUtil.urlEncode(state,'UTF-8') + ',' + 
               EncodingUtil.urlEncode(country,'UTF-8');
        url += '&sensor=false&client='+Label.cs_Client_Key;
        
        //sig. calc
                             
        String privateKey = Label.cs_Geocode_Private_Key;
		privateKey = privateKey.replace('-', '+');
		privateKey = privateKey.replace('_', '/');
		
		Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
		Blob urlBlob = Blob.valueOf(url);
		Blob signatureBlob = Crypto.generateMac('HMacSHA1', urlBlob, privateKeyBlob);
		
		String signature = EncodingUtil.base64Encode(signatureBlob);
		signature = signature.replace('+', '-');
		signature = signature.replace('/', '_');
        url = 'https://maps.googleapis.com' + url + '&signature=' + signature;
        
        
        Http h = new Http(); 
        HttpRequest req = new HttpRequest();

        req.setHeader('Content-type', 'application/x-www-form-urlencoded'); 
        req.setHeader('Content-length', '0'); 
        req.setEndpoint(url); 
        req.setMethod('POST');
        
        
        Integer numOfResults = 0;
        Boolean nowLatLng = false;
        String lat = '';
        String lng = '';
        String status = '';
    	
    	String responseBody;
        HttpResponse res;
        
    	res = h.send(req);
        responseBody = res.getBody(); 
        GeocodeResponse geoResponse = GeocodeResponse.parse(responseBody);
        return geoResponse;

    } 
    /**
     * @Description callout to google geocode API to fetch data
     * @param address components
     */
	public static PlacesAPIResponse getPlaces(String placeString) {
        System.debug('placeString'+placeString);
        String url;
        url = '/maps/api/place/textsearch/json?query=';
        String lang = UserInfo.getLanguage();
        if(lang == 'de'){
            url = '/maps/api/place/textsearch/json?language=de&query=';
        }
		
		
        url += EncodingUtil.urlEncode(placeString,'UTF-8');
        // url += paramvalue;
        url += '&key='+Label.cs_Places_API_Key;
        
        url = 'https://maps.googleapis.com' + url;
        System.debug('URL :'+url);
        
        Http h = new Http(); 
        HttpRequest req = new HttpRequest();

        //req.setHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setHeader('Content-Type','application/json; charset=UTF-8');
        req.setHeader('Content-length', '0'); 
        req.setEndpoint(url); 
        req.setMethod('POST');
        
    	
    	String responseBody;
        HttpResponse res;
        
    	res = h.send(req);
        responseBody = res.getBody();  
        // responseBody = '{"html_attributions":[],"results":[{"formatted_address":"Tannenstraße, 85579 Neubiberg, Germany","geometry":{"location":{"lat":48.0746373,"lng":11.6649227},"viewport":{"northeast":{"lat":48.0759862802915,"lng":11.6662716802915},"southwest":{"lat":48.0732883197085,"lng":11.6635737197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"89c1389b0a38e5c1febccc368f1724ce22761c60","name":"Tannenstraße","place_id":"ChIJY1Skx-_gnUcRzLmRtWumWKM","reference":"CmRbAAAACygopRPCd-zLjcV7Z_ZGvc4SWMi2nNFEt6R4lu7tAntClWQ_dfjg110tF4ZPt3pIH-1j69O7iF7jsNllmQGmWiTS0oJxuY7yzK4d4FJuptMoqm3YLAEFcKBqdeSeDVJ9EhD6zyBjnbQg6Jbz8MFOXs0gGhR-zwAv6W7oFVw6GWliwuWQm25a0A","types":["route"]},{"formatted_address":"Tannenstraße, 85640 Putzbrunn, Germany","geometry":{"location":{"lat":48.0778545,"lng":11.701703},"viewport":{"northeast":{"lat":48.0792034802915,"lng":11.7030519802915},"southwest":{"lat":48.0765055197085,"lng":11.7003540197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"be4c3bb96c5ccce32bb6fbb298fb202460eb36d0","name":"Tannenstraße","place_id":"ChIJp_l63wvhnUcR02SC9VHhOQE","reference":"CmRbAAAAFwgHqUM9wZcsq5U6LTVM8tOyoJp3qhX7RcmzKeQK5L-nhIidH59o180tT1m1CPTjMO7uAu9n9pV7SGxH2rk52dbuNoYStq4Qk_cQoDcML-J_oUXWT0-vcdjFCF5de57JEhAdA8OGmBns6UrIOOwI9I_wGhTY-aBRaytzoXOAwur2CuaKN-J_2w","types":["route"]},{"formatted_address":"Tannenstraße, 85649 Brunnthal, Germany","geometry":{"location":{"lat":47.9857767,"lng":11.7115109},"viewport":{"northeast":{"lat":47.9871256802915,"lng":11.7128598802915},"southwest":{"lat":47.9844277197085,"lng":11.7101619197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"c02463596da0484b26751bcad786d24f22d115df","name":"Tannenstraße","place_id":"ChIJtUBclsTlnUcRKqJ5mG2GjXE","reference":"CmRbAAAAtsWez6IqNJIXrVe5T_wekjj6VyyUl6fgiNlZM1X3z_2MVd5F0ji369J7Veiy1yBMa7u7Zobmy-mWmxlePXzFFcZLDjKMuwlq-USj3_ZJAto6qDD3S_UOqwJiAQKyCdPbEhD2aoA7yMSPblzV6EOV3eaQGhS9yDYTEp1-g_30c9PM8gWAQbZazA","types":["route"]},{"formatted_address":"Tannenstraße, 82041 Oberhaching, Germany","geometry":{"location":{"lat":48.01515690000001,"lng":11.592162},"viewport":{"northeast":{"lat":48.01650588029151,"lng":11.5935109802915},"southwest":{"lat":48.01380791970851,"lng":11.5908130197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"fd9e31a00e3d604e31307715663f0fef0f32b028","name":"Tannenstraße","place_id":"ChIJYUYiG4PdnUcR7ivD4BcEqtY","reference":"CmRbAAAAwO8js1HEsdAOB6fyfGBiZP78M-0tzcxH5n_BHQKPL4h20JWwk-Y71wAD8VKPlAtg3k5BchLBQcm6yK_bsXhpSx3Fa6ZlJk3m3us1VJgi9kEJvgHeHeP1ORpXutmtaI41EhDDVYDPKFOY8Na_4wHgvZgKGhQpazM1mSaJnXSCN7dcQ343R6PJ6A","types":["route"]},{"formatted_address":"Tannenstraße, 85609 Aschheim, Germany","geometry":{"location":{"lat":48.17454540000001,"lng":11.7117971},"viewport":{"northeast":{"lat":48.1758943802915,"lng":11.7131460802915},"southwest":{"lat":48.1731964197085,"lng":11.7104481197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"4abb553e786bfde65326d8301a5dc658ff439cc9","name":"Tannenstraße","place_id":"ChIJOfNDWw4LnkcRGQufBqoSdaM","reference":"CmRbAAAAlDNHPUbfgawcVDrmlJNWBsUpp58BwbQAYaJNYDPHz7R4wJItNJ0NbKHmQ6wCdbTbcIX7Y_JEll5xb7QvY9bv_VXAAXlSRv5fW3VAOgr-rvqd2hGf90lKVFFcib6llsOaEhAKvdtHWCS3nonQc30zbqDnGhRrKDePzmko3nsdkJWVXtGTCKl2ZQ","types":["route"]},{"formatted_address":"Tannenstraße, 82049 Pullach im Isartal, Germany","geometry":{"location":{"lat":48.0475329,"lng":11.5067625},"viewport":{"northeast":{"lat":48.0488818802915,"lng":11.5081114802915},"southwest":{"lat":48.0461839197085,"lng":11.5054135197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"b1cb00f3afdf7610e3c9a8286b51ac7d21883f8f","name":"Tannenstraße","place_id":"ChIJudTIw_bbnUcRyrhVIgDdDG0","reference":"CmRbAAAA98KH1hMNyIwhsVnt8zs9UssX4RwYYnJONUw5w2rhpOf-HnDyo11HFHPyENqmzdO5G42tgpSUgLzYOyAFe7S8MvktIF9HxTzSYpQSXKzXhRzIgfqHLkYmqBTBoAtYIi3HEhDdhKUaHTRRJDJZAivgDtBXGhQoDRMtC6ZBSbxfhBwPJ5HrJMw0Ng","types":["route"]},{"formatted_address":"Tannenstraße, 82065 Baierbrunn, Germany","geometry":{"location":{"lat":48.02931900000001,"lng":11.4851431},"viewport":{"northeast":{"lat":48.0306679802915,"lng":11.4864920802915},"southwest":{"lat":48.0279700197085,"lng":11.4837941197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"f720e03afb5ef7422d6fcc5932661062dd8a55ca","name":"Tannenstraße","place_id":"ChIJi_Tb5pvbnUcRPow0Oo86ypA","reference":"CmRbAAAAWnvavu7mm8FcDphIzdGhM0BCQtECyOXS2at3ddJpb5WxE3QEiVuITqIWDWKN8K7HCRjJhAFVX-OCP-jlxCtT_0J4fyK3akYdLgxnEo8Zg4QbLIxGBdn0XT1yg2O0HThlEhAEUTZJOFRcJu1Kdx-y6q6RGhSxv0XTlj_2SNlme852XcRDoAaNAg","types":["route"]},{"formatted_address":"Tannenstraße, 85716 Unterschleißheim, Germany","geometry":{"location":{"lat":48.27646240000001,"lng":11.5848975},"viewport":{"northeast":{"lat":48.27781138029151,"lng":11.5862464802915},"southwest":{"lat":48.27511341970851,"lng":11.5835485197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"9226173b87a4125457b6b0c88bd80246340979a2","name":"Tannenstraße","place_id":"ChIJm3b0butxnkcRDn5YgO4K76w","reference":"CmRbAAAAwMzKUqBgTSYIhzQiP6zEtOS0uE3XtaDWD6AP6YpfozgQXNVBL1wJd9R28bKI4Rrw5oBAJ1qL3ufWeGvl9srWdunI4KezbIkykbBvcNYrAvbrA_dK2t0-UVQeeOANfQmJEhB18gxKpob06gm3O3lHMMWaGhS_RaMkLpWcDLSRT-5BQSUrTmNyDg","types":["route"]},{"formatted_address":"Tannenstraße, 85764 Oberschleißheim, Germany","geometry":{"location":{"lat":48.2567402,"lng":11.562517},"viewport":{"northeast":{"lat":48.25808918029149,"lng":11.5638659802915},"southwest":{"lat":48.25539121970849,"lng":11.5611680197085}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png","id":"59b34a96cdfddaef2fa2c8fc28b854a4abe886f7","name":"Tannenstraße","place_id":"ChIJI-_g4QlxnkcR14H_XPpc2Ik","reference":"CmRbAAAAAq7JFo7wzgGhOIVpVdhA1zVFxWB5JV8JriGQTHl8i7Z0c6ty0NVrLA80nxZEbkGtl3kt3jKECjKhvhsPXVaomPYsBHoDJTX-cq1Q3L13ecKHeVMO_rSc5x13z9kk0wXCEhAd77WcPSILL78y4YPCOQEhGhRUWj65Pr89lFmleB6K3SnmPkcnzw","types":["route"]}],"status":"OK"}';
        System.debug('responseBody 2'+res.getHeaderKeys());
        System.debug('responseBody '+responseBody);
        PlacesAPIResponse placeResponse = PlacesAPIResponse.parse(responseBody);
        return placeResponse;

    }
    
    public static String getPlaceResponse(String placeId,String lang){
        String url;
        url = '/maps/api/place/details/json?placeid=';
        
        if(lang == 'de'){
            url = '/maps/api/place/details/json?language=de&placeid=';
        }
		
        url += EncodingUtil.urlEncode(placeId,'UTF-8');
        url += '&key='+Label.cs_Places_API_Key;
        
        url = 'https://maps.googleapis.com' + url;
        
        
        Http h = new Http(); 
        HttpRequest req = new HttpRequest();

        req.setHeader('Content-type', 'application/x-www-form-urlencoded'); 
        req.setHeader('Content-length', '0'); 
        req.setEndpoint(url); 
        req.setMethod('POST');
        
    	
    	String responseBody;
        HttpResponse res;
        
    	res = h.send(req);
    	responseBody = res.getBody(); 
    	return responseBody;
    }
   
    /**
     * @Description callout to google geocode API to fetch data for a place
     * @param placeId from google
     */
	public static List<PlaceDetailsAPIResponse> getPlaceDetails(String placeId) {
        List<PlaceDetailsAPIResponse> responseList = new List<PlaceDetailsAPIResponse>();
        String responseBody = '';
        String lang = UserInfo.getLanguage();
        // if(lang == 'de'){
        //     responseBody = getPlaceResponse(placeId,lang);
        //     PlaceDetailsAPIResponse placeDetailResponse = PlaceDetailsAPIResponse.parse(responseBody);
        //     responseList.add(placeDetailResponse);
        //     responseBody = getPlaceResponse(placeId,'de');
        //     placeDetailResponse = PlaceDetailsAPIResponse.parse(responseBody);
        //     responseList.add(placeDetailResponse);
        // }else{
            responseBody = getPlaceResponse(placeId,lang);
            // responseBody = '{"results" : [{"address_components" : [{"long_name" : "1600","short_name" : "1600","types" : [ "street_number" ]},{"long_name" : "Test","short_name" : "Test","types" : [ "floor" ]},{"long_name" : "Amphitheatre Pkwy","short_name" : "Amphitheatre Pkwy","types" : [ "route" ]},{"long_name" : "Mountain View","short_name" : "Mountain View","types" : [ "locality", "political" ]},{"long_name" : "Santa Clara County","short_name" : "Santa Clara County","types" : [ "administrative_area_level_2", "political" ]},{"long_name" : "California","short_name" : "CA","types" : [ "administrative_area_level_1", "political" ]},{"long_name" : "United States","short_name" : "US","types" : [ "country", "political" ]},{"long_name" : "94043","short_name" : "94043","types" : [ "postal_code" ]}],"formatted_address" : "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA","geometry" : {"location" : {"lat" : 37.4224764,"lng" : -122.0842499},"location_type" : "ROOFTOP","viewport" : {"northeast" : {"lat" : 37.4238253802915,"lng" : -122.0829009197085},"southwest" : {"lat" : 37.4211274197085,"lng" : -122.0855988802915}}},"place_id" : "ChIJ2eUgeAK6j4ARbn5u_wAGqWA","types" : [ "street_address" ]}],"status" : "OK"}';
            system.debug('responseBody:: '+responseBody);
            PlaceDetailsAPIResponse placeDetailResponse = PlaceDetailsAPIResponse.parse(responseBody);
            system.debug('parse:: '+placeDetailResponse);
            responseList.add(placeDetailResponse);
        //}
        
        return responseList;

    }
}