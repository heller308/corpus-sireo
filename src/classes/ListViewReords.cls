public class ListViewReords{
	public cls_columns[] columns;
	public String developerName;	//CSMA__A70_0_KM_All_Records
	public boolean done;
	public String id;	//00B6F00000Bw6h3
	public String label;	//70.0 KM All Records
	public cls_records[] records{get;set;}
	public Integer size;	//12
	public class cls_columns {
		public String fieldNameOrPath;	//Name
		public String value{get;set;}	//a006F00002ndUgC
	}
	public class cls_records {
		public cls_columns[] columns{get;set;}
	}
	public static ListViewReords parse(String json){
		return (ListViewReords) System.JSON.deserialize(json, ListViewReords.class);
	}
}