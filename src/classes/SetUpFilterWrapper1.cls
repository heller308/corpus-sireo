public class SetUpFilterWrapper1{
    public boolean defaultColor;
	public String color;	//#1a7a49
	public cls_condition condition;
	public class cls_condition {
		public String field;	//CSMA__cs_Country__c
		public String type;	//PICKLIST
		public String fieldLabel;	// Country 
		public String operator;	//=
		public String operatorLabel;	//equals
		public String value;	//Germany
		public String valueLabel;	//Germany
	}
	public String objName;	//csma__object_offers__c
	public String fieldsetName;	//CSMA__testFieldSet
	public static SetUpFilterWrapper1 parse(String json){
		return (SetUpFilterWrapper1) System.JSON.deserialize(json, SetUpFilterWrapper1.class);
	}
}