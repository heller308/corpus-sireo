public with sharing class VerifyListView_Ctlr {
	public String flag{get;set;}
	public Id pendingLVId{get;set;}
	public VerifyListView_Ctlr() {
		flag = 'false';
		PageReference currentPage = ApexPages.currentPage();
		if(currentPage!=null){
			Map<String,String> urlParams = currentPage.getParameters();
			if(urlParams.get('id')!=null && urlParams.get('id')!=''){
				pendingLVId = urlParams.get('id');		
				try{
    				CSMA__Pending_List_View__c obj = [SELECT Id,Name,CSMA__Existing_Record__c,CSMA__Google_Location__c,CSMA__Distance__c,CSMA__User_Id__c FROM CSMA__Pending_List_View__c WHERE id =:pendingLVId];
    				if(obj!=null){
                        System.debug('$$%%$$%%$$%%');
    				    flag = createListView(obj.CSMA__Existing_Record__c,obj.CSMA__Google_Location__c,obj.Name,obj.CSMA__Distance__c,obj.CSMA__User_Id__c);
    				}
				}catch(Exception e){
				    flag = 'Already Created';
				    System.debug('Data  :'+e.getStackTraceString());
                    System.debug('Line  :'+e.getLineNumber());
				    System.debug('Data  :'+flag);
				}
				// if(flag){
				// 	Utility.deleteListView(obj.Id);
				// }
			}
		}
	}

	public String createListView(String initialLocationString,String stringForGoogleLocation,String listViewName,Decimal distance,String uid){
        ControllingData_Hlpr setupFieldData = new ControllingData_Hlpr();
        setupFieldData = Utility.fillMappingValue();
        List<CSMA__SetupData__c> setup = [SELECT Id,CSMA__Search_Radius__c,CSMA__Radius_Unit__c FROM CSMA__SetupData__c LIMIT 1];
        if((String.isNotBlank(initialLocationString) || String.isNotBlank(stringForGoogleLocation)) && setupFieldData.geoCodeLatitude!='' && setupFieldData.geoCodeLongitude!='' && setupFieldData.objectName!=''){
            Map<String, Object> meta = String.isNotBlank(initialLocationString)?(Map<String, Object>) JSON.deserializeUntyped(initialLocationString):null;
            Decimal fieldLat = meta!=null?Decimal.valueof(String.valueof(meta.get(setupFieldData.geoCodeLatitude))):Decimal.valueof(string.valueof(((Map<String, Object>) JSON.deserializeUntyped(stringForGoogleLocation)).get('Lat')));
            Decimal fieldLong = meta!=null?Decimal.valueof(string.valueof(meta.get(setupFieldData.geoCodeLongitude))):Decimal.valueof(string.valueof(((Map<String, Object>) JSON.deserializeUntyped(stringForGoogleLocation)).get('Long')));
            
            Decimal radius = distance;
            
            String unit = setup!=null?(string.valueOf(setup[0].CSMA__Radius_Unit__c)!=null?(String.valueOf(setup[0].CSMA__Radius_Unit__c)=='Miles'?'MI':'KM'):'KM'):'KM';
            
            String listViewFilterField = setupFieldData.geoCodeLatitude.replace('Latitude__s','c');
            String modifiedLVName = listViewName.replace(' ','_').replace('.','_').replace(',','_');
            
            String salesforceBasicUrl = String.valueof(URL.getSalesforceBaseUrl().toExternalForm()).split('\\.')[1];
            salesforceBasicUrl = salesforceBasicUrl+'.salesforce.com/';
            
            HTTP h = new HTTP();
            HTTPRequest req = new HTTPRequest();
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction', 'create');
            
            String b = '<?xml version="1.0" encoding="UTF-8"?>';
            b += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
            b += '<soapenv:Header>';
            b += '<ns1:SessionHeader soapenv:mustUnderstand="0" xmlns:ns1="http://soap.sforce.com/2006/04/metadata">';
            b += '<ns1:sessionId>' + UserInfo.getSessionId() + '</ns1:sessionId>';
            b += '</ns1:SessionHeader>';
            b += '</soapenv:Header>';
            b += '<soapenv:Body>';
            b += '<create xmlns="http://soap.sforce.com/2006/04/metadata">';
            b += '<metadata xsi:type="ns2:ListView" xmlns:ns2="http://soap.sforce.com/2006/04/metadata">';
            //This is the API name of the list view
            b += '<fullName>'+setupFieldData.objectName+'.'+'A'+modifiedLVName+'</fullName>';
            b += '<booleanFilter>1</booleanFilter>';
            //Columns you want to display
            b += '<columns>NAME</columns>';
            // b += '<columns>CSMA__cs_Asset_Class__c</columns>';
            // b += fieldsetFields;
            //Filterscope should be set to Everything for every one to be able to access this List view
            b += '<filterScope>Everything</filterScope>';
            // Enter the filter that you want to set
            b += '<filters>';
            b += '<field>'+listViewFilterField+'</field>';
            b += '<operation>within</operation>';
            b += '<value>'+fieldLat+':'+fieldLong+':'+radius+':'+unit+'</value>';
            b += '</filters>';
            b += '<label>'+listViewName+'</label>';
            b+= '<sharedTo>null</sharedTo>';
            b += '</metadata>';
            b += '</create>';
            b += '</soapenv:Body>';
            b += '</soapenv:Envelope>';
           
            req.setBody(b);
            req.setCompressed(false);
            // Set this to org's endpoint and add it to the remote site settings.
            req.setEndpoint('https://'+salesforceBasicUrl+'services/Soap/m/25.0');
            HTTPResponse resp = h.send(req);
            if(resp.getStatusCode() == 200 && resp.getStatus()=='OK'){
                System.debug('Creted'+uid);
                List<String> uids = new List<String>();
                uids.add(uid);
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = uids;
                message.optOutPolicy = 'FILTER'; 
                message.subject = Label.cs_Map_app_List_view_created;
                String html = '';
                String notifyString = Label.cs_Your_list_view_name_of_list_view_was_created;
                //notifyString = notifyString.replaceAll('NAME_OF_USER',UserInfo.getName());
                notifyString = notifyString.replaceAll('NAME_OF_LIST',listViewName);
                String LDSStyle = 'display: inline-block;margin-bottom: 12px;border-radius: .25rem;line-height: 1.875rem;text-decoration: none;padding-left: 1rem;padding-right: 1rem;vertical-align: middle;background-color: #0070d2;border: 1px solid #0070d2;color: #fff;';
                html  += '<a href="https://'+salesforceBasicUrl+'/a00"  style="'+LDSStyle+'">'+Label.cs_Go_to_list_view+'</a>';
                html += '<br>'+notifyString;
                
                System.debug('HTML :'+ html);
               message.htmlBody = html;
               // message.plainTextBody = 'This is the message body.';
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

                if (results[0].success) {
                    System.debug('The email was sent successfully.');
                } else {
                    System.debug('The email failed to send: ' + results[0].errors[0].message);
                }
                return 'true';
            }
            System.debug('Not Creted');
            Utility.trackErrorAfterCallout(resp.getBody());
            
            return 'false';
        }else{
            Utility.trackError('Permission Error');
            return 'false';
        }
    }

	@RemoteAction
	public static void deletePendingLV(String lvId){
		Utility.deleteListView(lvId);
	}
}