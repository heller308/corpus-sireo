public class Utility{
    
    public static String getSetupDataLabel(String type, String fieldName){
        String label;
        List<CSMA__Geocding_and_Actions__c> csList = CSMA__Geocding_and_Actions__c.getall().values();
        if(!csList.isEmpty()){
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(csList[0].CSMA__Controlling_Object__c);
            if(type == 'Object'){
                List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{fieldName}); // this can accept list of strings, we describe only one object here
                label = describeSobjectsResult[0].getLabel();
            }else if(type == 'FieldSet'){
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldName);
                label = fieldSetObj.getLabel();
            }else if(type == 'Field'){
                Map<String, Schema.SObjectField> fieldMap = SObjectTypeObj.getDescribe().fields.getMap();
                label = fieldMap.get(fieldName).getDescribe().getLabel();
            }
            return label;
        }else{
            return null;
        }
    }
    
    public static ControllingData_Hlpr fillMappingValue(){
        ControllingData_Hlpr setupFieldData = new ControllingData_Hlpr();
        List<CSMA__Geocding_and_Actions__c> objList = CSMA__Geocding_and_Actions__c.getall().values();
        
        if(!objList.isEmpty()){
            setupFieldData = new ControllingData_Hlpr(
                objList[0].CSMA__Controlling_Object__c,
                objList[0].CSMA__New_PropertyVF__c,
                objList[0].CSMA__Street__c,
                objList[0].CSMA__ZIP_Code__c,
                objList[0].CSMA__City__c,
                objList[0].CSMA__State__c, 
                objList[0].CSMA__Country__c,
                objList[0].CSMA__Name__c,
                objList[0].CSMA__Geocode_Status__c,
                objList[0].CSMA__Latitude__c,
                objList[0].CSMA__Longitude__c,
                objList[0].CSMA__Geocode_Quality__c,
                objList[0].CSMA__Geocode_Error__c,
                objList[0].CSMA__Info_Window_Field_Set__c,
                objList[0].CSMA__House_Number__c);
        }else{
            setupFieldData = null;
        }
        return setupFieldData;
    }

    @future
    public static void trackErrorAfterCallout(String errMessage){
        Integer strLength = errMessage.length();
        if(strLength >499)
            errMessage = errMessage.trim().substring(0,499);
        else
            errMessage = errMessage.trim();
       CSMA__Error_Tracking__c err = new CSMA__Error_Tracking__c(CSMA__Message__c=errMessage);
       insert err;
    }
    
    @future
    public static void deleteListView(Id pendingListviewId){
       CSMA__Pending_List_View__c obj = [SELECT Id FROM CSMA__Pending_List_View__c WHERE Id= :pendingListviewId];
       delete obj;
    }

    public static void trackError(String errMessage){
        Integer strLength = errMessage.length();
        if(strLength >499)
            errMessage = errMessage.trim().substring(0,499);
        else
            errMessage = errMessage.trim();
       CSMA__Error_Tracking__c err = new CSMA__Error_Tracking__c(CSMA__Message__c=errMessage);
       //insert err;
    }
    
    public static String getFieldType(String objectName,String fieldName){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objectName);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Schema.DescribeFieldResult ftype =  fieldMap.get(fieldName).getDescribe();
        return String.valueOf(ftype.getType());
    }
    
    public static String getFieldLabel(String objectName, String fieldName, String type){
        String label;
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        if(type == 'Object'){
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{fieldName}); // this can accept list of strings, we describe only one object here
            label = describeSobjectsResult[0].getLabel();
        }else if(type == 'Field'){
            Map<String, Schema.SObjectField> fieldMap = SObjectTypeObj.getDescribe().fields.getMap();
            label = fieldMap.get(fieldName).getDescribe().getLabel();
        }
        return label;
    }
    
    @RemoteAction
    public static String getFieldLabelRemote(String objectName, String fieldName, String type1){
        String label;
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        if(type1 == 'Object'){
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{fieldName}); // this can accept list of strings, we describe only one object here
            label = describeSobjectsResult[0].getLabel();
        }else if(type1 == 'Field'){
            Map<String, Schema.SObjectField> fieldMap = SObjectTypeObj.getDescribe().fields.getMap();
            label = fieldMap.get(fieldName).getDescribe().getLabel();
        }else if(type1 == 'Fieldset'){
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldName);
            label = fieldSetObj.getLabel();
        }
        return label;
    }
    
    
    public static  List<FiltersWrapper> availableFields(String ObjectName){
        List<FiltersWrapper> options = new List<FiltersWrapper>();
        if(ObjectName != ''){
            DescribeSObjectResult DesSObjectResult = Schema.getGlobalDescribe().get(ObjectName).getDescribe();
            if(CheckPermissionUtility.checkAllPermissionsObject(DesSObjectResult)){
                Map<String, Schema.SObjectField> fieldMap =  DesSObjectResult.fields.getMap();
                for (String fieldName : fieldMap.keySet()) {
                        String label = fieldMap.get(fieldName).getDescribe().getLabel();
                        String name = fieldMap.get(fieldName).getDescribe().getName();
                        if(CheckPermissionUtility.checkAllPermissionsField(fieldMap.get(fieldName).getDescribe())){
                            options.add(new FiltersWrapper(name,label,Utility.getFieldType(ObjectName,name)));        
                        }
                }    
            }else{
                Utility.trackError('Permission issue in availableFields()(SetupData_Ctlr)');
            }
        }
        return options;
    }
    public class FiltersWrapper{
        public String apiName {get;set;}
        public String label {get;set;}
        public String type {get;set;}
        public FiltersWrapper(){
            this.apiName = '';
            this.label = '';
            this.type = '';
        }
        public FiltersWrapper(String apiName,String label,String type){
            this.apiName = apiName;
            this.label = label;
            this.type = type;
        }
    }
}